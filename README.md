## Sample project of UserWay Continuous-Accessibility with GitLab CI Docker Image.

This project uses the [Docker Image](https://hub.docker.com/repository/docker/userway/cicd-gitlab) for GitLab pipelines to trigger the Continuous-Accessibility analysis.

The GitLab CI Docker Image is configured to build and analyze all branches and pull requests. See [this PR](https://gitlab.com/userway/cicd-gitlab-sample/-/merge_requests/2) as an example.

### Continuous-Accessibility Dashboard view

![img.png](https://gitlab.com/userway/cicd-gitlab-sample/-/raw/main/img.png?ref_type=heads)
