import type { Config } from "@userway/cicd-core";

export default {
  organization: "alex-userway-org",
  project: "cicd-gitlab-sample",
  token: process.env.USERWAY_TOKEN,
  reportPaths: ["./uw-a11y-reports"]
} satisfies Config;
